#include "EquipeMoto.h"
#include "Personne.h"

EquipeMoto::EquipeMoto(const std::string& nom, const Personne& manager) :
	nom_(nom),
	manager_(manager)
{
}

EquipeMoto::~EquipeMoto()
{
}

const std::string& EquipeMoto::getNom() const
{
	return nom_;
}

void EquipeMoto::setNom(const std::string& nom)
{
	nom_ = nom;
}

const Personne& EquipeMoto::getManager() const
{
	return manager_;
}

void EquipeMoto::setManager(const Personne& manager)
{
	manager_ = manager;
}
