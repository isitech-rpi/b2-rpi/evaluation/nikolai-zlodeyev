#ifndef PERSONNE_H
#define PERSONNE_H

#include <string>

class Personne {
private:
    std::string nom;

public:
    Personne(const std::string& nom);

    std::string getNom() const;
};

#endif
