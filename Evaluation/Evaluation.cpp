#include <iostream>
#include "Personne.h"
#include "EquipeMoto.h"

void AffichePersonne(const Personne& p) {
    std::cout << "Personne(" << &p << "): " << p.getNom() << std::endl;
}

Personne* CreerPersonne() {
    std::string nom;
    std::cout << "Prénom Personne2: ";
    std::cin >> nom;
    return new Personne(nom);
}

int main() {
    Personne pilote_1("Fabio");

    AffichePersonne(pilote_1);

    Personne* pilote_2 = CreerPersonne();

    AffichePersonne(*pilote_2);

    delete pilote_2;

    Personne manager("Jarvis");
    EquipeMoto equipe_1("YMF", manager);

    std::cout << "Nom de l'équipe : " << equipe_1.getNom() << std::endl;
    std::cout << "Manager de l'équipe : " << equipe_1.getManager().getNom() << std::endl;
    std::cout << "Adresse equipe : " << &equipe_1 << std::endl;

    return 0;
}