#include <iostream>
#include <string>
#include "Personne.h"

class EquipeMoto {
public:
	EquipeMoto(const std::string& nom, const Personne& manager);
	~EquipeMoto();

	const std::string& getNom() const;
	void setNom(const std::string& nom);

	const Personne& getManager() const;
	void setManager(const Personne& manager);

private:
	std::string nom_;
	Personne manager_;
};
