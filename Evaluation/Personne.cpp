#include "Personne.h"

Personne::Personne(const std::string& nom) : nom(nom) {}

std::string Personne::getNom() const {
    return nom;
}