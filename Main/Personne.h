#include <iostream>

class Personne {
public:
	Personne(const std::string& nom);
	~Personne();

	const std::string& getNom() const;
	void setNom(const std::string& nom);

private:
	std::string nom_;
};

Personne::Personne(const std::string& nom) : nom_(nom) {
	std::cout << "Personne cree nommee " << nom_ << std::endl;
}

Personne::~Personne() {
	std::cout << "Destruction de la personne nommee " << nom_ << std::endl;
}

const std::string& Personne::getNom() const {
	return nom_;
}

void Personne::setNom(const std::string& nom) {
	nom_ = nom;
}

int main() {
	Personne pilote_1("Fabio");

	std::cout << "Adresse pour : " << &pilote_1 << std::endl;
	std::cout << "Nom de pilote_1 : " << pilote_1.getNom() << std::endl;

	return 0;
}
